import {Injectable} from '@angular/core';
import {CsvReaderService} from "./csv-reader.service";
import {XmlReaderService} from "./xml-reader.service";
import {FileType} from "../models/enum/FileType";
import {InvalidTransactionReport} from "../models/InvalidTransactionReport";
import {TransactionMapperService} from "./transaction-mapper.service";
import {Transaction} from "../models/Transaction";
import {TransactionValidationService} from "./transaction-validation.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
@Injectable({
  providedIn: 'root'
})
export class HandleFileUploadService {
  errorMessages: String[];

  constructor(
    protected csvFileReader: CsvReaderService,
    protected xmlFileReader: XmlReaderService,
    protected transactionMapper: TransactionMapperService,
    protected transactionValidation: TransactionValidationService,
    protected httpClient: HttpClient
  ) { }

  /**
   * ProcessLocally locally processes the file (reading, mapping, validation) and returns an InvalidTransactionReport
   * @param file
   */
  async processLocally(file:File) : Promise<InvalidTransactionReport> {
    this.errorMessages = [];

    let records: string[][];

    if (this.checkFileExtension(file.name) === FileType.CSV) {
      records = await this.csvFileReader.readFile(file);
    } else if (this.checkFileExtension(file.name) === FileType.XML) {
      records = await this.xmlFileReader.readFile(file);
    }

    const transactions : Transaction[] = this.transactionMapper.mapRecords(records);

    return this.transactionValidation.validateTransactions(transactions);
  }

  /**
   * Process remote sends the file to the api for processing and returns with a report of the invalid transactions
   * @param file
   */
  async processRemote(file: File) : Promise<InvalidTransactionReport> {
    this.errorMessages = [];
    try {
      return await this.uploadFile(file);
    } catch (e) {
      this.errorMessages.push(e.message);
      return null;
    }
  }

  /**
   * uploadFile uploads the file to the api
   * @param file
   */
  private uploadFile(file: File) : Promise<InvalidTransactionReport> {
    const endpoint = environment.api_url + '/upload';
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this.httpClient.post<InvalidTransactionReport>(endpoint, formData).toPromise();
  }

  /**
   * check file extension checks the file extension of the provided file and returns the filetype
   * @param filename
   */
  private checkFileExtension(filename: String) : FileType {
    if(filename.endsWith('.csv')) {
      return FileType.CSV;
    } else if (filename.endsWith('xml')) {
      return FileType.XML;
    } else {
      this.errorMessages.push("Unknown filetype supplied, allowed filetypes are CSV and XML");
    }
  }
}
