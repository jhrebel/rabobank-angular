import {Injectable} from '@angular/core';
import {Transaction} from "../models/Transaction";
import {Fields} from "../models/Fields";
import {Mutation} from "../models/Mutation";
import {MutationType} from "../models/enum/MutationType";

@Injectable({
  providedIn: 'root'
})
export class TransactionMapperService {

  constructor() { }

  /**
   * Map the string records to an array of transactions for further handling
   * @param records
   */
  mapRecords(records: string[][]) : Transaction[] {
    return records.map(record => {
      if(record.length === Fields.NUMBER_OF_FIELDS) {
        return this.recordToTransaction(record);
      }
    });
  }

  /**
   * Map a single record to a transaction
   * @param record
   */
  recordToTransaction(record: string[]) : Transaction {
    const transaction = new Transaction();
    transaction.transactionReference = parseInt(record[Fields.TRANSACTION_REFERENCE], 10);
    transaction.accountNumber = record[Fields.ACCOUNT_NUMBER];
    transaction.description = record[Fields.DESCRIPTION];
    transaction.startingBalance = parseFloat(record[Fields.START_BALANCE]);
    transaction.endBalance = parseFloat(record[Fields.END_BALANCE]);

    const mutation = new Mutation();
    if(record[Fields.MUTATION].charAt(0) === '+') {
      mutation.mutationType = MutationType.ADDITION;
    } else {
      mutation.mutationType = MutationType.DEDUCTION;
    }
    mutation.amount = parseFloat(record[Fields.MUTATION].substr(1));
    transaction.mutation = mutation;
    return transaction;
  }
}
