import {Injectable} from '@angular/core';
import {Transaction} from "../models/Transaction";
import {InvalidTransactionReport} from "../models/InvalidTransactionReport";
import {MutationType} from "../models/enum/MutationType";
import {InvalidTransaction} from "../models/InvalidTransaction";
import {error} from "util";
import {throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TransactionValidationService {
  static THRESHOLD : number = .00001;
  constructor() { }

  /**
   * Validates all the transactions for end balance and uniqueness of the transaction reference
   * @param transactions
   */
  public validateTransactions(transactions : Transaction[]) : InvalidTransactionReport {
    const invalidTransactionReport = new InvalidTransactionReport();
    invalidTransactionReport.invalidTransactionList = [];
    for(let i = 0; i < transactions.length; i++) {
      let errorMessages = [];

      try {
        this.validEndBalance(transactions[i]);
      } catch (error) {
        errorMessages.push(error.message);
      }


      try {
        this.uniqueTransactionReference(transactions, i);
      } catch (error) {
        errorMessages.push(error.message);
      }

      if(errorMessages.length > 0) {
        const invalidTransaction = new InvalidTransaction();
        invalidTransaction.errorMessages = errorMessages;
        invalidTransaction.transaction = transactions[i];
        invalidTransactionReport.invalidTransactionList.push(invalidTransaction);
      }
    }

    return invalidTransactionReport;
  }

  /**
   * Validates the end balance of a transaction
   * @param transaction
   */
  private validEndBalance(transaction : Transaction) : void {
    let expectedEndBalance;

    if(transaction.mutation.mutationType === MutationType.ADDITION) {
      expectedEndBalance = transaction.startingBalance + transaction.mutation.amount;
    } else {
      expectedEndBalance = transaction.startingBalance - transaction.mutation.amount;
    }

    if(Math.abs(expectedEndBalance - transaction.endBalance) > TransactionValidationService.THRESHOLD) {
      throw new Error(`End balance invalid, expected: ${expectedEndBalance}, received: ${transaction.endBalance}`);
    }
  }

  /**
   * Checks if a transaction is unique by checking for a matching transactionReference number
   * @param transactions
   * @param transactionIndex
   */
  private uniqueTransactionReference(transactions : Transaction[], transactionIndex: number) : void {
    if(transactionIndex === 0) {
      return;
    }
    // using a for loop as I dont want to loop over the entire array, only up until the transaction we are checking for.
    // findIndex is also a possibility if you don't mind possibly looping of the entire array
    for( let i = 0; i < transactionIndex; i++) {
      if(transactions[i].transactionReference === transactions[transactionIndex].transactionReference) {
        throw new Error(`Duplicate entry: record number ${(transactionIndex+1)} is duplicate of record number ${(i+1)}`);
      }
    }
  }
}
