import { Injectable } from '@angular/core';
import xml2js from 'xml2js';
import {FileReaderBase} from "../models/FileReaderBase";

@Injectable({
  providedIn: 'root'
})
export class XmlReaderService extends FileReaderBase {

  constructor() {
    super();
  }

  /**
   * Reads in an xml file and using the xml2js library converts it into an array of string arrays
   * @param file
   */
  async readFile(file: File): Promise<string[][]> {
    return super.readFile(file);
  }

  /**
   * File specific processing for the xml file
   * @param reader
   * @param resolve
   * @param reject
   */
  fileSpecificProcessing(reader, resolve, reject) {
    const parser = new xml2js.Parser();

    // turn the xml string into a json object
    parser.parseString(reader.result, (err, result) => {
      const records : string[][] = [];

      resolve(result.records.record.map(record => {
        return [
          record.$.reference,
          record.accountNumber[0],
          record.description[0],
          record.startBalance[0],
          record.mutation[0],
          record.endBalance[0]
        ];
      }));
    });
  }
}
