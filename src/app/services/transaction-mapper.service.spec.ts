import { TestBed } from '@angular/core/testing';

import { TransactionMapperService } from './transaction-mapper.service';
import {Transaction} from "../models/Transaction";

describe('TransactionMapperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TransactionMapperService = TestBed.get(TransactionMapperService);
    expect(service).toBeTruthy();
  });

  it('should map string arrays to transactions', () => {
    const service: TransactionMapperService = TestBed.get(TransactionMapperService);
    const records : string[][] = [["234234234", "NL234234ABN234", "Test description", "100.0", "-10.1", "89.9"]];
    const transactions : Transaction[] = service.mapRecords(records);
    expect(transactions.length).toEqual(1);
    expect(transactions[0].transactionReference).toEqual(234234234);
  });
});
