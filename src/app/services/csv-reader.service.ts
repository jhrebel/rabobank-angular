import { Injectable } from '@angular/core';
import {FileReaderBase} from "../models/FileReaderBase";

@Injectable({
  providedIn: 'root'
})
export class CsvReaderService extends FileReaderBase {
  constructor() {
    super();
  }

  /**
   * Reads in the csv file
   * @param file
   */
  readFile(file: File): Promise<string[][]> {
    return super.readFile(file, 'CP1252');
  }

  /**
   * File specific processing for the csv file
   * @param reader
   * @param resolve
   * @param reject
   */
  fileSpecificProcessing(reader : FileReader, resolve, reject) : void {
    let records : string[][] = [];
    let csvData = <string> reader.result;
    let csvLines = csvData.trim().split(/\r\n|\n/);

    // remove the headers
    csvLines.shift();

    // split on the comma delimiter
    for(let i = 0; i < csvLines.length; i++) {
      records.push(csvLines[i].split(/,/));
    }

    resolve(records);
  }
}
