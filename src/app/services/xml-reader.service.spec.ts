import { TestBed } from '@angular/core/testing';

import { XmlReaderService } from './xml-reader.service';

describe('XmlReaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: XmlReaderService = TestBed.get(XmlReaderService);
    expect(service).toBeTruthy();
  });
});
