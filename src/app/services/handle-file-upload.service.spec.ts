import { TestBed } from '@angular/core/testing';

import { HandleFileUploadService } from './handle-file-upload.service';
import {HttpClient, HttpHandler} from "@angular/common/http";

describe('HandleFileUploadService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HttpClient,
      HttpHandler
    ]
  }));

  it('should be created', () => {
    const service: HandleFileUploadService = TestBed.get(HandleFileUploadService);
    expect(service).toBeTruthy();
  });
});
