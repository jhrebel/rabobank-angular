import { TestBed } from '@angular/core/testing';

import { TransactionValidationService } from './transaction-validation.service';
import {Transaction} from "../models/Transaction";
import {Mutation} from "../models/Mutation";
import {MutationType} from "../models/enum/MutationType";
import {InvalidTransactionReport} from "../models/InvalidTransactionReport";

describe('TransactionValidationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TransactionValidationService = TestBed.get(TransactionValidationService);
    expect(service).toBeTruthy();
  });

  it('should give an empty invalidTransactionReport for a valid transaction list', () => {
    const service: TransactionValidationService = TestBed.get(TransactionValidationService);
    const transaction : Transaction = new Transaction();
    transaction.transactionReference = 123123123;
    transaction.accountNumber = "NL3ABNA23434";
    transaction.description = "test description";
    transaction.startingBalance = 10.0;
    transaction.mutation = new Mutation(10.0, MutationType.ADDITION);
    transaction.endBalance = 20.0
    let invalidTransactionReport : InvalidTransactionReport = service.validateTransactions([transaction]);

    expect(invalidTransactionReport.invalidTransactionList.length).toEqual(0);
  });

  it('should give a filled invalidTransactionReport for an invalid transaction list', () => {
    const service: TransactionValidationService = TestBed.get(TransactionValidationService);
    const transaction : Transaction = new Transaction();
    transaction.transactionReference = 123123123;
    transaction.accountNumber = "NL3ABNA23434";
    transaction.description = "test description";
    transaction.startingBalance = 10.0;
    transaction.mutation = new Mutation(10.0, MutationType.DEDUCTION);
    transaction.endBalance = 20.0
    let invalidTransactionReport : InvalidTransactionReport = service.validateTransactions([transaction]);

    expect(invalidTransactionReport.invalidTransactionList.length).toEqual(1);
  });
});
