import {Component, OnInit} from '@angular/core';
import {HandleFileUploadService} from "../../services/handle-file-upload.service";
import {ValidationLocation} from "../../models/enum/ValidationLocation";
import {InvalidTransactionReport} from "../../models/InvalidTransactionReport";

@Component({
  selector: 'app-upload-form',
  templateUrl: './upload-form.component.html',
  styleUrls: ['./upload-form.component.scss']
})
export class UploadFormComponent {
  public uploadInProgress = false;
  public locations = ValidationLocation;
  public validationLocation: ValidationLocation;
  public invalidTransactionReport: InvalidTransactionReport;
  private file: File;

  constructor(protected handleFileUploadService: HandleFileUploadService) {
  }

  /**
   * HandleFormSubmission decides where to process the file and sets the invalid transaction report when ready
   */
  async handleFormSubmission() {
    this.invalidTransactionReport = null;

    if(this.validationLocation === ValidationLocation.CLIENT_SIDE) {
      this.invalidTransactionReport = await this.handleFileUploadService.processLocally(this.file);
    } else {
      this.uploadInProgress = true;
      this.invalidTransactionReport = await this.handleFileUploadService.processRemote(this.file);
      this.uploadInProgress = false;
    }
  }

  /**
   * HandleFileInput sets the file variable to the uploaded file
   * @param files
   */
  async handleFileInput(files: FileList) {
    this.file = files[0];
  }
}
