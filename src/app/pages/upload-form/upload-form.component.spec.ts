import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UploadFormComponent} from './upload-form.component';
import {FormsModule} from "@angular/forms";
import {InvalidTransactionsReportComponent} from "../../components/invalid-transactions-report/invalid-transactions-report.component";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {ValidationLocation} from "../../models/enum/ValidationLocation";
import {By} from "@angular/platform-browser";

describe('UploadFormComponent', () => {
  let component: UploadFormComponent;
  let fixture: ComponentFixture<UploadFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadFormComponent, InvalidTransactionsReportComponent ],
      imports: [FormsModule],
      providers: [HttpClient, HttpHandler]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
