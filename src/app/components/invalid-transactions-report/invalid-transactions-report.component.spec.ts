import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvalidTransactionsReportComponent } from './invalid-transactions-report.component';

describe('InvalidTransactionsReportComponent', () => {
  let component: InvalidTransactionsReportComponent;
  let fixture: ComponentFixture<InvalidTransactionsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvalidTransactionsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvalidTransactionsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
