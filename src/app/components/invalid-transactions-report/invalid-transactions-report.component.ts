import {Component, Input, OnInit} from '@angular/core';
import {InvalidTransactionReport} from "../../models/InvalidTransactionReport";

@Component({
  selector: 'app-invalid-transactions-report',
  templateUrl: './invalid-transactions-report.component.html',
  styleUrls: ['./invalid-transactions-report.component.scss']
})
export class InvalidTransactionsReportComponent implements OnInit {
  @Input() invalidTransactionsReport: InvalidTransactionReport;

  constructor() { }

  ngOnInit() {
  }
}
