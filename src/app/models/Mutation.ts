import {MutationType} from "./enum/MutationType";

export class Mutation {
  amount: number;
  mutationType: MutationType;

  constructor(amount?: number, mutationType?: MutationType) {
    this.amount = amount | 0;
    this.mutationType = mutationType | 0;
  }
}
