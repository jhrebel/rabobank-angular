import {Mutation} from "./Mutation";

export class Transaction {
  transactionReference: number;
  accountNumber: string;
  description: string;
  startingBalance: number;
  endBalance: number;
  mutation: Mutation;
}
