import {InvalidTransaction} from "./InvalidTransaction";

export class InvalidTransactionReport {
  invalidTransactionList: InvalidTransaction[];
}
