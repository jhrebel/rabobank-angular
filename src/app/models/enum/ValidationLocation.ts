export enum ValidationLocation {
  CLIENT_SIDE = 1,
  SERVER_SIDE = 2
}
