import {Transaction} from "./Transaction";

export class InvalidTransaction {
  transaction: Transaction;
  errorMessages: String[];
}
