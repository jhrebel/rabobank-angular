export class Fields {
  static TRANSACTION_REFERENCE : number = 0;
  static ACCOUNT_NUMBER : number = 1;
  static DESCRIPTION : number = 2;
  static START_BALANCE : number = 3;
  static MUTATION : number = 4;
  static END_BALANCE: number = 5;
  static NUMBER_OF_FIELDS: number = 6;
}
