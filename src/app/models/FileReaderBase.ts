import {IFileReader} from "../interfaces/IFileReader";

export class FileReaderBase implements IFileReader {
  /**
   * Read in the file
   * @param file
   */
  readFile(file: File, characterEncoding?: string): Promise<string[][]> {
    return new Promise<string[][]>((resolve, reject) => {
      const reader : FileReader = new FileReader();

      if(characterEncoding) {
        reader.readAsText(file, characterEncoding);
      } else {
        reader.readAsText(file);
      }
      reader.onload = () => {
        this.fileSpecificProcessing(reader, resolve, reject)
      }
    });
  }

  /**
   * Function to handle the processing of the specific file
   * @param reader
   * @param resolve
   * @param reject
   */
  fileSpecificProcessing(reader, resolve, reject) {
    throw new Error('I shouldn\'t be called');
  }
}
