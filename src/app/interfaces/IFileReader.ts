export interface IFileReader {
  readFile(file: File, characterEncoding?:string): Promise<string[][]>;
}
